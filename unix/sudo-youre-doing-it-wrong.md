Sudo: You're doing it wrong
Michael W Lucas

Va a hablar de:

* Sudo y sudoers
* Editar y probar sudoers
* Listas y alias%
* Opciones y condiciones bases%
* Secuencias de escape% y editores
* Configuración
* Entornos de consola%
* Sudo como SDI%
* Políticas complejas
* LDAP
* Registro% y depuración%

Cuando se sepa de una característica, luego se puede averiguarla.

Sudo deja a los usarios ejecutar comandos específicos como otro usario.

Lo bueno de sudo es que es multiplataforma y funciona dentro del modelo tradicional de privilegios% de Unix.

Hay otras herramientas de separación de privilegios tales como listas de control de acceso (ACL), selinux, pfexec, y rols.

Su problema es que todas sólo funcionan en ciertos plataformas.

# Evitar sudo

Si se peude hacer sin sudo, no lo uses. Por ejemplo, en Unix puedes usar grupos para dar permisiones a usarios. Mucha gente (yo incluso) no se entera de los tres bytes en el centro de la cadena de carácters de permisiones. En este caso sólo se usa sudo para gestionar servicios.

# La política predeterminada

Así parece:

```
%wheel ALL=(ALL) ALL
```

Significa que todos en el grupo `wheel`, puedes hacer todo con sudo.

Algunos sistemas operativos modernos como Ubuntu se aporta sin contraseña de «root». Esta bien mientras no rompes sudo.

Michael nos avisa que sería una buena idea poner una contraseña de «root» para tener acceso por puerta trasera en caso de urgencia.

# ¿Por qué subdividir privilegio?

Así son las ventajas:

* Respnsibilidad
* Culpa
* Protección
* Privilegios con o sin contraseña
* Hacer que el jefe haga su trabajo

En todo sistema grande y complejo hay personas multiples que se encargan de partes diversas, y cada una necesitará acceso a «root»% por alguna parte del sistema en su cargo. Adminstradores de bases de datos no necesitan acceso a «root» de las impresoras% y vice versa.

Si ya se ha subdividido el privilegio, tu jefe ya no puede acercarte y decir «sé que usted no es el DBA pero ¿te importa entrar un momento y comprobar esta sola cosa para mi?» No es posible que tal acontecimiento te podría volver a morderte el culo...

# Formato% del reglas

Así parece:

```
usario servidor=comando
```

* Usario - nombre, grupo, gid, uid, o lista
* Servidor - dirección IP, nombre, o lista
* Comando - comando inidividuo o una lista

Por ejemplo:

```
mwlucas dns1=ALL
```

dice que el usario mwlucas en el servidor dns1 puede ejecutar cualquier comando.

# Listas

Se separan artículos% con coma%, lineas largas con barra invertida%.

Por ejemplo:

```
mwlucas,pkdick dns1,dns2 = \
  /sbin/service named, /sbin(service syslogd
```

dice que los dos usarios on los dos servidores pueden ejecutar los dos comandos.

Para plasmar el concepto sin la sintaxis% de lista, se necesitaría seis lineas.

# Negación

Se pueden negar usarios y servidores.

```
%wheel,!mwlucas ALL = ALL
```

También con comandos pero no se uses porque superarlo es sencillo - simplemente copias el comando a un nuveo archivo.

Para superar esta negación:

```
mwlucas ALL = ALL,!/bin/bash
```

Usas estos comandos:

```
cp /bin/sh /tmp/myscript
sudo /tmp/myscript
```

# Executar como otro usario

Más sintaxis:

```
mwlucas db1 (oracle) ALL
```

dice que el usario en el servidor puede ejecutar cualquier comando por cuenta de oracle.

# Política de sudo

EL proceso parece a lo del cortafuegos:

* Se procesan reglas en orden
* Gana la última regla que corresponda
* Se debe terminar con una línea vacía%.

# Editar la política

Usa visudo para editar /etc/sudoers. Hace una copia temporal del archivo política, te deja editar la copia

###

sudo -l para comprobar tu acceso

sudo -U usario -l para comprobar el eccaso de otro

patrones apra machear

"" para prohibir opciones

Dar nombre a listas de toods tips con Cmnd_Alias, User_Alias y Host_Alias

De dónde salen los nobres reconocidos? De LDAP, NIS, UID, GID, Active Directory. Hosts de netgroups, direccion IP

Defaults insults

passwd_tries
passwd_timeout=0 para desabilitar el caché de contraseña

lecture=always, lecture_files

hasta preventing escapes

NOEXEC para deshaibilitar ejecutar comandos desde comandos. por ejemplo teclear ! para ejecutar otro comando en more.

Cita /bin/newaliases como comando que lanza nuevos procesos en segundo plano.

# sudoedit

Si quieres que usarios puedan editar alugunos archivos predeterminados, pon el `sudoedit` comando. `sudoedit` lanza el editor nombrado en la variable `$EDITOR`.

```
mwlucas ALL = sudoedit /etc/rc.conf
```

# Variables de entorno

sudo quita todas las variables excepto una lista blanca corta. Él da un ejemplo de SSH para disponibilizar% su config de agente al entorno sudo.

```
Defaults env_keep+="HOME SSH_CLIENT SSH_COMMECTION SSH_TTY SSH_AUTH_SOCK"
```

Usando sudo como SDI: sudo comprueba la suma de control% de la copia de `su` y si no sale igual del valor precalculado, te rechaza.

# Registro y depuración

sudo puede registrar y depurar todo que teclee en una sesión.

Se puede reproducir la sequencia de comandos en tiempo real, accelerado o más lento.

sudo funciona con PAM. Se puede eliminar contraseñas y usr Google Authenticator o SSH agent en su lugar.

¿Por qué dejar de usar contraseñas? La red avemaría ("Hail Mary").
