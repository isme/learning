# Chuleta de SSH

Ver el hash md5 del clave público (se usa en Github por ejemplo).

```
ssh-keygen -f pubkey -l -E md5
```
