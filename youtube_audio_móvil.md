#

Me gusta escuchar charlas por internet para aprender más de las tecnologías y técnicas, y bien para conocer los sabios actuales de mi campo. Y para mí no hay mejor lugar para hacerlo que en casa donde puedo quedarme tranquilo con pantalla y cascos y una taza de té para concentrarme bien en el tema y apuntar las notas.

Pero también quiero aprovechar el viaje entre hogar y empresa, una buena media hora, para seguir el aprendizaje. Claro que en metro no puedo sacar mi portatil, así que tuve que solucionarlo con mi móvil. Desgraciadamente, tampoco puedo simplemente eschuarlo en móvil al bajarlo en tiempo real. Aunque fuera posible mantenerse contectado todo el viaje a la red 3G, gastar tantos datos sería muy caro en función de dinero y. Además, de todas formas, sería un desperdicio de datos porque el lector Youtube siempre baja el video cuando sólo quieres eschucar el audio.

No quiería quedarme rayado con intentar guardar el video entero en un caché frágil tras bajarlo por guifi en casa. Tampoco aguanto las apps que salen en Play Store que prometen solucionarlo pero en realidad te acosten con anuncios o te piden dinero para habilitar todos sus características.

Entonces, que hice?

# Instalar brew

[brew](http://brew.sh/) es un prerequísito para instalar todo el software que sigue. Sigue las direcciones para instalar brew.

#  Bajar desde YouTube

Primero, bajé el video con la herramienta [youtube-dl](http://rg3.github.io/youtube-dl/). O lo puedes instalarlo desde su web, o en Mac usar brew.


```
brew install youtube-dl
```

# Sacar el audio

https://www.ffmpeg.org/

```
brew install ffmpeg
```



```
brew cask install android-file-transfer
```
