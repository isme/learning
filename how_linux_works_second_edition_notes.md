# How Linux Works

All references from before Chapter 3 unless noted.

# Book Recommendations

* The Devil, The Gnu, and The Penguin
* Operating System Concepts
* Modern Operating Systems
* The Linux Command Line
* UNIX for the Impatient
* Learning the UNIX Operating System
* Mastering Regular Expressions
* Programming Perl (Regular Expressions chapter)
* Introduction to Automata Theory, Languages, and Computation
* Learning the vi and Vim Editors: Unix Text Processing
* GNU Emacs Manual

# Commands 

* diff -u (unified diff)
* pwd -P (avoid symlinks)
* man -k (search man pages)
* ps -ww (to assume inifite screen width)
* ls -l (to view the setuid bit)
* umask 022 (to all people to read my new files)
* umask 077 (to deny all access to my new files)

