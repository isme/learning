AWS Webinar Spanish: Buenas Prácticas de seguridad en la nube

con Rubén Ruiz y David Sanz

# Model de seguridad compartida

* "Vosotros elegís el tipo de cifrado"
* Los datas se quedan donde los pongaís

# Gestión de identidad

* Encripción por defecto en S3, EBS, RDS, Redshift
* KMS, IAM
* Siempre que haya un "deny" tiene mayor peso que un "allow"
* PARC - principal, acción, recurso, condición
* Federación de identidad (www.amazon.com, graph.facebook.com, accounts.google.com)
* "NotAction" no es igual de "Deny"
* Condiciones
* Acceso a su propio cubo: ${aws:username}

# Buenas prácticas

* Crear usarios individuos
* Permitir el priviligio mínimo
* Gestionar los permisos con grupos
* Restringir más con condiciones
* Habilitar AWS CloudTrail para obtener el registro de llamadas a la API
* Configurar una política fuerte de contraseña
* Cycle creds
* 2FA
* Use IAM roles to share access
* Use roles for Amazon EC2 instances
* Remove or restrict use of root

![](!assets/2017-03-28_buenas_prácticas_de_seguridad_en_la_nube-ad260.png)

* Federated v AWS users
* Invalidar credenciales temporales
* Políticas empotradas v gestionadas
* ¿Una sola cuenta o multiples cuentas?

# Características

* Infraestructura como código
* CloudFormation
* Split ownership - app team owns core section?
* AWS CodeDeploy AppSpec file

# Logging

* Máxima: Si se mueve, ¡grábalo!
* Logs de infraestructura, de servicios y de hosts

# CloudTrail Best Practices

* Enable CloudTrail in all regions
* Enable log file validation
* Encypted logs
* Integrate with CloudWatch logs
* Centralize logs from all accounts

# VPC Flow Logs

* Captar datos al nivel de interfaz de red

* AWS Config Rules
* Lambdas Crons to check to rules

* Amazon Inspector
* AWS Trusted Advisor

* Requesting a pen test
