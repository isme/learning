# Optimizing the Data Tier for Serverless Web Applications

Prahlad Rao, Balaji Iyer

Performance tip: don't close the connection in the lambda handler function. Container reuse means that on the next invocation you can reuse the open connection.

Keep the function warm by calling the lambda on a timer with cloudwatch.
