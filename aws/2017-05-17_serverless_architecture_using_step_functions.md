# Server Orchestration with AWS Step functions

Rahul xxx and Andy Katz

Missed the intro for this one

* From monoliths to microservices to functions
* Flow control of functions

Custom coordination solutions: chaining, databases, queues

Desirable properties:

* Scales out
* Doesn't lose state
* Handles errors/time outs
* Easy to build/operate
* Auditable

# Introducing step functions

* Amazon States Language to define a graph of functions
* The graph is not accessible through the API; everything else in the console is.
* API: Create, StartExecution, StopExecution, List, Delete
* States; Task, Choice, Parallel, Wait, Fail, Succeed, Pass
* Use cases: Workflow/Order management, batch processing, shell-script replacement, enterprise application management, data processing, human approval tasks
* Demo

# Serverless Application Model

* Extension of CloudFormation
* Write, Pack, Create change set, Deploy
* Continuous delivery with CodeStar


# Ideas

* "With S3 deletion is a one way trip." True if versioning is enabled?
* Unit testing https://serverless.zone/unit-and-integration-testing-for-lambda-fc9510963003?gi=e57210d4ac33