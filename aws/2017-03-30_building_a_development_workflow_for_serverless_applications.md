# Building a Development Workflow for Serverless Applications

Chris Munns, Senior Developer Advocate

Recording: https://www.youtube.com/watch?v=e3lreqpWN0A&feature=youtu.be

Slides: https://www.slideshare.net/AmazonWebServices/building-a-development-workflow-for-serverless-applications-march-2017-aws-online-tech-talks

# Serverless Architecture

* No management of provisioning, scaling, idle time, fault tolerance
* Lambda's deployable unit is the function
* Almost every event in an amazon service can trigger a lambda

# Model your resources

* CloudFormation is the go-to service for modelling
* Serverless Application Model (SAM) is an extension for CF optimized for serverless
* A more concise way to express AWS resources
* Allows Function, Api, and SimpleTable resources + normal CF elements
* New `aws cloudformation` commands: `package` and `deploy`

# Configure multiple environments

* Testing in isolation
* Same account, different stacks v multiple accounts
* Consider organization to manage different accounts

# Establish you testing/validation model

* Lean on existing unit testing tools for your chosen language
* Deployment package is your zipped code + dependencies
* CodeBuild is a pay as your go jenkins-like service that produces build packages
* Arbitrary commands are possible, all defined in YAML

* CodePipeline has CloudFormation actions!
* Testing tools: landscape.io, codeclimate, coverall, localstack, runscope, ghostinspector


# Automate your delivery process

* Demo of aws-serverless-samfarm (works in free tier)

Ideas

* Build a continuous integration pipeline for CloudFormation templates
* Learn more about SAM (github labs)
* Check out samfarm
* Seperate accounts for developers
* Auto create CF templates on every commit
* Check out step functions
* With a SAM template you can refer to a file of environment parameters to be injected into the resources as CF parameters. A replacement for the params stack?
