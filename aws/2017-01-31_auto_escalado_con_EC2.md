2017-01-31 Auto escalado con EC2

Carlos Sanchiz, AWS Architect

Bienvenidos al seminario web

Tipos de escalado:

* escalado vertical
* escalado horizontal

assets/2017-01-31_auto_escalado_con_EC2-a70ef.png

"una vista muy por encima"

balanzador de carga

un pico de carga

ELB tiene opción de "drain connections"

hay muchas políticas de terminación per primero la priodidad es de equilibriar las zonas de disponibilidad

# ELB

no sana, no healthy

se puede subir el límite máximo en el caso de rembalanzar tras reinstanciar una zona de disponibilidad. nunca va a poner el número debajo del mínimo ni del deseado.

escalados planificados (por crontab o eventos individuales)

la alarma se activa cuando se cruza el umbral

recollectión de métricas

la primera «rotura», si me permites la palabra

la brecha (del umbral)

Políticas escalonadas v simples

assets/2017-01-31_auto_escalado_con_EC2-202ce.png

usar un hook para registrarse nuevas instances en DNS

assets/2017-01-31_auto_escalado_con_EC2-20d44.png

Disculpas para las que nos han quedado en el tintero.
