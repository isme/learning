# ElasiCache Deep Dive: Best Practices and Usage Patterns

Michael Labib

## Redis data structures

* String
* Set
* Sorted Set
* List
* Hash
* Lua scripts
* Geospatial (3.2)
* Pub/sub

# Use patterns

* Caching
* Use lambda to transform the data on the way from DB to cache (also do "data decoration")
* Write through v lazy load, pros and cons
* Session caching, move session storage from EC2 (so you don't lose it if the instance terminates)
* Streaming data enrichment

# Cluster features

* Automatic client-side sharding
* When single primary shard fails
* When majority of primary shards fail
* Have an odd number of shard to make recovery faster

# Monitoring

* Lost connection :-(
* "russian doll caching"

# Caching strategies

* Store in db (map object to row) and store in cache (serialize object to string)
* Generate test data with mockaroo
* Demo: github mikelabib amazon-elasticache-redis-caching-demos

Takeaway: caching can not only increase application performance but also reduce database cost. Choose an appropriate caching architecture for your workload.
