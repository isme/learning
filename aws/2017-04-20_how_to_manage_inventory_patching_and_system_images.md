# How to Manage Inventory, Patching and System Images for your Hybrid Cloud with AWS Management Capabilities

Taylor Anderson, Sr Product Manager, Amazon EC2 Systems Manager

Automate, monitor, and comply

Amazon EC2 Systems manager works in EC2 and "on-prem" - anything outside EC2?

Capabilities:

* Run Command (Check next month's session with Ananth)
* State Manager
* Maintenance Windows
* Parameter Store
* Automation
* Inventory
* Patch Manager

# Automation

* AMI building is a pain point
* AWS-* are documents managed by AWS. For example AWS-UpdateLinuxAmi
* Demo of creating an AMI with default parameters, paraeter store, and using a lambda to get the latest AMI
* Service role in IAM is required - check the documentation for more examples
* Can generate CloudWatch Events

# Inventory

* Running inventory requires SSMAgent
* Generates a snapshot of inventory which can be stored in AWS Config
* "Managed Instances" shows instances with SSMAgent installed and with the correct roles (you can now attach roles to existing instances)
* Demo of browsing the installed packages on a managed instance in the EC2 console
* Demo of collecting inventory for a set of tagged instances
* You can push custom inventory using put-inventory API or doing a scheduled write to a known location
* Demo of filtering instances by an installed component version
* Demo of AWS config tracking changes over time
* Demo of using rule-based compliance checks across instances (blacklisted application)

# Patching

* First version supports only Windows (but Linux "requests have been heard")
* Define baseline, maintenance windows, and check compliance
* Use tags to define patch groups (Tag:PatchGroup?)
* Demo of creating a patch baseline, maintenance window, registering a task with the window

# Ideas

* Learn how to install SSMAgent on Ubuntu (in EC2 and outside)
* Try AWS-UpdateLinuxAmi to update the golden AMI
* Try patch manager to ensure all instances are up to date
* Try parameter store to distribute secrets (such as SSH keys)
* Use AWS Inventory to collect info about differences in on-prem instances
