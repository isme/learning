Best Practices for Managing Security Operations in AWS

Armando Leite, Principal Security Architect

https://attendee.gotowebinar.com/recording/viewRecording/1132632597761973250/536449905791247619/iain@wakoopa.com
https://www.youtube.com/watch?v=KuOy63yzc8c
https://www.slideshare.net/AmazonWebServices/best-practices-for-managing-security-operations-in-aws-march-2017-aws-online-tech-talks

* Minimum Security Baseline v Pro-level
* Amazon released the Cloud Adaption Framework
* Security types: Directive, Preventive, Detective, Responsive
* "The SOP is code" - standard operating procedure

# Control

* IAM Best practices (remember previous talk)
* One account v multiple accounts (repeat)
* Segmented account structure (repeat)
* Use Organizations to centrally manage multiple accounts
* CloudFormation > Unicorns + Rainbows > Security? No
* Use continuous delivery to keep doing the right thing (fail fast)
* Phases: Commit, Acceptance, Capacity/Integration/Staging, Production

# Monitor

* Set up CloudTrail in all regions
* Secirty account architeture for aggregrating Cloudtrail from multiple accounts
* AWS Config and Config Rules are separate services
* Configs can be used to evaluate policies on resources

# Fix

* Returning to the known good state
* Increasing awareness, indirect action, direct action (automatic)
* Flow Log event flow demo
* If the logon to an EC2 instance is not OK, we can automatically enable enhanced surveillance
* Automatically isolate, deregister, and snapshot the instance for forensic analysis

awslabs automating governance sample
blog implementing devsecops using aws codepipeline
whitepaper aws security best practices
AWS re:Invent Security Jam at the next summit
