# Active Archiving with Amazon S3 and Tiering to Glacier

Recording: https://www.youtube.com/watch?v=W_DdWDUzjSk

Slides: https://www.slideshare.net/AmazonWebServices/active-archiving-with-amazon-s3-and-tiering-to-amazon-glacier-march-2017-aws-online-tech-talks

Marc Trimuschat, AWS Storage Services

* Block, file, and object storage services
* Cloud data migration services

# Active Archiving

* Typically big files, media for example, that don't change. But they do need to be quickly accessible.
* SoundCloud: originals streamed from S3, originals stored in Glacier
* Digital Globe: 100PB images, new 1PB every year
* Philips Healthcare: 15PB of petient data archived beyond patient lifetime using HIPAA compliant methods
* "BAA" "business asociate" https://aws.amazon.com/es/compliance/hipaa-compliance/
# Storage classes

* (also covered in Deep Dive S3 webinar)
* Standard, Infrequent Access, Amazon Glacier
* You will probably use Infrequent Access for most data, standard only for "hot" data
* Data lifecycle management tools
* S3 analytics
* Tagging objects with different security and retention policies according to your compliance requirements

# Glacier

* Vaults, Archives etc
* A replacement for tape-based archival systems
* 11 nines in S3 v 5 nines with traditional on-site + off-site copies
* "Built-in fixity checking" ?
* Expedited retrieval: "last minute play-out schedule swap"
* Standard retrieval: "disaster recovery"
* Bulk retrieval: "PB scale re-transcoding or video/image analysis"
* Glacier VaultLock for undeletable data

Data import:

* Direct connect
* Snowball (Edge has on-board compute)
* Storage gateway for hybrid solutions
* "Hierarchical storage manager" - generalization of leveldb?
* Sony had a "120GB PRORES FILE" -> video codec standard

Ideas:

* Learn about "fixity checking"
* Move all "cold" S3 data to Infrequent Access
* Watch Amazon S3 and Glacier Deep Dive talks from re:Invent (get links in slides)
