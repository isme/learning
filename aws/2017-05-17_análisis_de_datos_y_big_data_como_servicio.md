# Análisis de datas en AWS. Big Data como servicio.

Javier Ros, Solution Architect

https://attendee.gotowebinar.com/recording/viewRecording/8355847849155704065/6029381300847506956/iain@wakoopa.com

* Qué es big data
* El ciclo de vida del dato
* Almacenamiento: steam, file, DW, search, SQL, NoSQL, cache. en función del caso de uso
* S3 y glacier: Datos calientes, templados, y fríos

# Ingestión

* Deep Learning AMI for Amazon Linux
* Estatíticas de vistas de páginas de Wikipedia
* Copiar los zips de pagecounts a un bucket de S3
* Crear una tabla en dynamodb de los wikistats
* Ingestión batch ("por lote"?)
* Ingestión en tiempo real

# Transformación

* Amazon EMR open source o de MapR
* En EMR "Se puede incluir RStudio de una forma muy sencilla"
* Demo: Crear un cluster
* "Los ahorros de spot son bastante importantes"
* Usar EMRFS para acceder a los objetos de S3 como fueron ficheros en HDFS
* Con datos en S3 el cluster se escala en funciñon del procesamiento en vez del tamaño de datos (como puro Hadoop)
* Athena con ANSI SQL
* Demo de EMR con Zeppelin - se puede hacer queries en scala y sql, visualizando los resultados de varias maneras
* Step de EMR usando un jar para escribir el resultado a dynamodb

# Visualización

* QuickSight (como Tableau)

# Vocablos

* "ancho de banda de datos"
* "datos en bruto"
* "ficheros planos"
