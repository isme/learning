# Scaling Up Tenfold with Amaon EC2 Spot Instances

Chad Schmutzer, Solutions Architect - EC2 Spot Instances

# Spot Instances

* Consumption models: on-demand, reserved, spot market
* You never pay more than market price or more than what your bid
* You might be kicked off in two minutes if you are out-bid
* Instance pools within a region divided by family, size, tenancy model, AZ

# Best Practices

* Statelessness
* Fault tolerance
* Multi-AZness
* Loose coupling
* Instance flexibility (can run on different instance types)

# Spot Blocks

* Cloudhealth survey found that 50% of all insatnces run less than 6 hours
* A block can run up to 6 hours
* Save up to 50% off on-demand pricing

# Spot Fleet

* Automate the procurement of instances at the lowest price
* Now supports autoscaling
* Use Spot Bid Advisor to estimate outbidding frequency
* get_spot_xxx python script
* EC2 Spot Avisor in console to recommend a fleet
* Shutdown time available through EC2 metadata service

# Example Architectures

* Queue-based processing
* Batch processing
* Live-streaming content
* Stateless web application
* Hosting ECS (containers)

# Demo

* CF template to launch an autoscaled app over a spot fleet

# Ideas

* Use spot blocks for data-science type machines
* Use spot fleet instance of on-demand autoscaling for a normal service
