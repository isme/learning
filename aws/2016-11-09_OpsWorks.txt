AWS OpsWorks Webinar November 2016

Amir Golan, Thomas Kaschwig

What is OpsWorks?

Allows you to define stacks and layers (application tiers)
Fill those with resources
Specify the resource lifecycle in OpsWorks
Associates Chef cookbooks with your instances, applied using life-cycle events

Main events are setup, configure, and deploy. Attach a recipe to each event.

Setup runs on instance boot.
Deploy event is used for updating (new software version for example)
Configure event triggered on state changes (such as a new instance in a cluster)

OpsWorks Access Policies can be used alongside IAM policies
Deny, Show, Deploy, Manage permissions
One click to grant SSH, sudo access for example
Temporary session management

Aggregate cloudwatch metrics grouped by stacks and layers

Captures the logs of chef runs
Integrate application logs with CloudWatch Logs

CodePipeline automatically builds and tests pushed code (like Codeship)
Intergrates with Github, Jenkins, Runscpe, Xebia, Apica, BlazeMeter, CLoudBees, more

Supports Chef 11 and Chef 12
You can use it to provision servers outside EC2
