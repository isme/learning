# Chuleta de vim

## Poner artefactos perdidos en Chef

Copie la lista de artefactos:

```
Missing artifacts: hostname,logrotate,rsyslog,fail2ban
```

Aplique esta comando para poner cada uno en su propia linea:

```
s/,/\r/
```

En modo visual de lineas:

```
s/\(.*\)/cookbook '\1', path: '..\/\1'/
```

## Convertir lineas en lista de Ruby/Python

```
%s/\(.*\)/  '\1',/
```
