# DNS and Bind Notes

A zone is just a section of the entire list of computer names on the internet.

BIND is the Berkeley Internet Name Domain software. It is the most popular
implementation of the DNS specs.

New system administrators should read chapters 1-7 then 12-14.

# Notes

The .es domain does not have a whois server.

```
$ whois amazon.es.
This TLD has no whois server, but you can access the whois database at
https://www.nic.es/
```

On the site (currently with insecure redirect to www.dominios.es) you can search for .es domains, but you have to pass a captcha.

www.allwhois.com now redirects to www.markmonitor.com

allwhois.org does not show real whois info for .es domain.

```
mail_addr = hostmaster.csiro.com

This form of Internet mail address is a vestige of two former DNS records, MB and MG. MB (mailbox) and MG (mail group) were supposed to be DNS records specifying Internet mailboxes and mail groups (mailing lists) as subdomains of the appropriate domain. MB and MG never took off, but the address format they would have dictated is used in the SOA record, maybe for sentimental reasons.
```

www.nameboy.com domain name generator and availability checker (nonesense but fun)

escoc.es, which I bought through Route 53 in AWS is was done through Gandi. They appear as a French registrar on interNIC's list of registrars. https://www.internic.net/origin.html

Is my network registered? It might need to be before a registrar will deligate a subdomain to it.

To check if a network is registered you can check a regional internet registry. The regional internet registry for Europe is RIPE Network Coordination Center (https://www.ripe.net/)

My IP address at the time of writing is 37.14.165.69. According to RIPE it is registered and assigned to Jazztel, whose registered office is in Pozuelo de Alarcón.

https://apps.db.ripe.net/search/query.html?searchtext=37.14.165.69

My name server version:

## Chapter 4

### Setting up zone data

Check the version of BIND (the format depends on the version)

```
$ named -v
BIND 9.10.3-P4-Ubuntu <id:ebd72b3>
```

A general rule for zone datafile design:

```
If a host is multihomed (has more than one network interface), create an address (A) record for each alias unique to one address and then create a CNAME record for each alias common to all the adresses.
```

To get the file of the root name servers:

* FTP to ftp.rs.internic.net
* Log in as user 'anonymous' (no password)
* cd to directory to domain
* get the file db.cache

The file published in the book is dated Jan 29, 2004. The version I downloaded is dated April 11, 2017. The number of root servers is the same - thirteen. Some of the IPv4 addresses have changed in the new version. All the root servers now have an AAAA record for IPv6 as well.
