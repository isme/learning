# La depuración de `kitchen create`

Vaya. Primera execución, primera problema:

```
$ kitchen create
-----> Starting Kitchen (v1.11.1)
-----> Creating <default-ubuntu-1404>...
[...]
>>>>>> ------Exception-------
>>>>>> Class: Kitchen::ActionFailed
>>>>>> Message: 1 actions failed.
>>>>>>     Failed to complete #create action: [Expected process to exit with [0], but received '1'
---- Begin output of vagrant up --no-provision --provider virtualbox ----
STDOUT:
STDERR: No usable default provider could be found for your system.

Vagrant relies on interactions with 3rd party systems, known as
"providers", to provide Vagrant with resources to run development
environments. Examples are VirtualBox, VMware, Hyper-V.

The easiest solution to this message is to install VirtualBox, which
is available for free on all major platforms.

If you believe you already have a provider available, make sure it
is properly installed and configured. You can see more details about
why a particular provider isn't working by forcing usage with
`vagrant up --provider=PROVIDER`, which should give you a more specific
error message for that particular provider.
---- End output of vagrant up --no-provision --provider virtualbox ----
Ran vagrant up --no-provision --provider virtualbox returned 1] on default-ubuntu-1404
>>>>>> ----------------------
>>>>>> Please see .kitchen/logs/kitchen.log for more details
>>>>>> Also try running `kitchen diagnose --all` for configuration

```

Ejecuté `vagrant up --provider virtualbox`, leí `.kitchen/logs/kitchen.log`, ejecuté `kitchen diagnose --all`; nada util.

Busqué en Google [vagrant virtualbox no usable default provider](https://www.google.es/search?sourceid=chrome-psyapi2&ion=1&espv=2&ie=UTF-8&q=vagrant%20virtualbox%20no%20usable%20default%20provider&oq=vagrant%20virtualbox%20no%20&aqs=chrome.1.69i57j0l5.8046j0j7), y salío esta respuesta en [Stack Overflow](http://stackoverflow.com/a/38355417/111424):

> Vagrant 1.8.4 and Virtualbox 5.1.X aren't compatible on MacOS 10.11 (can't set a provider).

Son mis mismísimas versiones:

```
$ vagrant --version
Vagrant 1.8.4
$ VBoxManage --version
5.1.6r110634
```

Otro [comenario](http://stackoverflow.com/questions/29450437/unable-to-vagrant-up-how-to-set-providers#comment64256347_38355417) dice:

> Vagrant 1.8.5 should work with 5.1 (just checked Vagrant's changelog on Github)

Brew no tiene método para actualizar una app ya instalada. No obstante en este caso sirve desinstalar y instalar de nuevo en seguida.

```
brew cask uninstall --force vagrant && brew cask install vagrant
brew cask uninstall --force virtualbox && brew cask install virtualbox
```

Y ahora están actualizadas:

```
$ vagrant --version
Vagrant 1.8.6
$ VBoxManage --version
5.1.8r111374
```

Y ahora con `kitchen create`:

```
$ kitchen create
-----> Starting Kitchen (v1.11.1)
-----> Creating <default-ubuntu-1404>...
       Bringing machine 'default' up with 'virtualbox' provider...
       ==> default: Box 'bento/ubuntu-14.04' could not be found. Attempting to find and install...
           default: Box Provider: virtualbox
           default: Box Version: >= 0
       ==> default: Loading metadata for box 'bento/ubuntu-14.04'
           default: URL: https://atlas.hashicorp.com/bento/ubuntu-14.04
       ==> default: Adding box 'bento/ubuntu-14.04' (v2.3.0) for provider: virtualbox
           default: Downloading: https://atlas.hashicorp.com/bento/boxes/ubuntu-14.04/versions/2.3.0/providers/virtualbox.box
==> default: Successfully added box 'bento/ubuntu-14.04' (v2.3.0) for 'virtualbox'!
       ==> default: Importing base box 'bento/ubuntu-14.04'...
==> default: Matching MAC address for NAT networking...
       ==> default: Checking if box 'bento/ubuntu-14.04' is up to date...
       ==> default: Setting the name of the VM: kitchen-motd_ubuntu-default-ubuntu-1404_default_1478009165279_28902
       ==> default: Clearing any previously set network interfaces...
       ==> default: Preparing network interfaces based on configuration...
           default: Adapter 1: nat
       ==> default: Forwarding ports...
           default: 22 (guest) => 2222 (host) (adapter 1)
       ==> default: Booting VM...
       ==> default: Waiting for machine to boot. This may take a few minutes...
           default: SSH address: 127.0.0.1:2222
           default: SSH username: vagrant
           default: SSH auth method: private key
           default:
           default: Vagrant insecure key detected. Vagrant will automatically replace
           default: this with a newly generated keypair for better security.
           default:
           default: Inserting generated public key within guest...
           default: Removing insecure key from the guest if it's present...
           default: Key inserted! Disconnecting and reconnecting using new SSH key...
       ==> default: Machine booted and ready!
       ==> default: Checking for guest additions in VM...
       ==> default: Setting hostname...
       ==> default: Machine not provisioned because `--no-provision` is specified.
       [SSH] Established
       Vagrant instance <default-ubuntu-1404> created.
       Finished creating <default-ubuntu-1404> (4m58.73s).
-----> Kitchen is finished. (4m59.81s)
```

¡Ganancia!
