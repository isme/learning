Learning Chef

Escribí una recetera que incluye una plantilla para añadir información sobre la máquina al archivo MOTD y una receta para rellenar los parametros y poner el archivo en la carpeta update-motd.d.

Depuré y solucioné una incompatibilidad de versiones entre Mac OS X, Vagrant, y VirtualBox.

Enumeré los instancías de pruebas en la cocina con `kitchen list`. Creé una instancia de prueba con `kitchen create`. Apliqué la recetera motd_ubuntu con `kitchen converge`. Comprobé su código de salida con `$?` a ver que hubo terminado sin error. Enteré en la instancia con `kitchen login` para ver el resultado. Destruí la instancia con `kitchen destroy`.

# Reto 3: Verifique que su recetera `motd_ubuntu` ejecute tanto en Ubuntu 12.04 como en Ubuntu 14.04.

Para verificar que también la recetera ejecutará en Ubuntu 12.04, añado su nombre como plataforma en el archivo `.kitchen.yml`.

```
platforms:                                                                      
  - name: ubuntu-12.04
  - name: ubuntu-14.04
```

Veo que otra instancia ahora sale en la lista de `kitchen list`:

```
$ kitchen list
Instance             Driver   Provisioner  Verifier  Transport  Last Action
default-ubuntu-1204  Vagrant  ChefZero     Busser    Ssh        <Not Created>
default-ubuntu-1404  Vagrant  ChefZero     Busser    Ssh        <Not Created>
```

Sigo los mismos pasos que antes y todo sale bien. Una diferencia importante es el comando para entrar en la instancia. Ya que hay dos instancias, tengo que nombrar la instancia en que quiero entrar.

```
$ kitchen login
>>>>>>
Argument `' returned multiple results:
  * default-ubuntu-1204
  * default-ubuntu-1404
```

```
$ kitchen login default-ubuntu-1204
Welcome to Ubuntu 12.04.5 LTS (GNU/Linux 3.2.0-110-generic x86_64)
[...]
hostname:  default-ubuntu-1204
fqdn:      default-ubuntu-1204
memory:    503308kB
cpu count: 1
Last login: Tue Nov  1 15:30:58 2016 from 10.0.2.2
vagrant@default-ubuntu-1204:~$ exit
logout
Connection to 127.0.0.1 closed.
```

Comentario sobre la guía para los autores:

En el tutorial no sale la clave `verifier` bajo la clave pricipale `suites`. La clave principal `verifier` sale comentada, pero en `chef generate cookbook` sale descomentada.

Se debería revisar esta parte:

> *provisioner* specifies how to run Chef. We use `chef_zero` because it enables you to mimic a Chef server environment on your local machine. This allows us to work with node attributes and data bags. This allows us to work with node attributes and other Chef server features.

Pendiente: Leer la documentación de la estructura del archivo .kitchen.yml http://docs.chef.io/config_yml_kitchen.html
