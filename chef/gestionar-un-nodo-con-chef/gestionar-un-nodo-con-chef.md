Siguiendo la guía [Get set up with Chef server from the AWS Marketplace](https://learn.chef.io/tutorials/manage-a-node/ubuntu/aws/set-up-your-chef-server/)

Lancé un servidor Chef desde el [mercadillo Amazon](https://aws.amazon.com/marketplace/pp/B01AMIH01Q?ref=cns_srchrow).

Una sorpresa: el sistema operativo no es Ubuntu, y no puedo entrarme por ssh con la clave y usario normal.

```
$ ssh -i ~/.ssh/ChefServer.pem ubuntu@54.171.39.128
Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
```

El resumen del mercadillo dice que en realidad es "Linux/Unix, CentOS 7.2".

Por lo menos pude acceder a la consola para ver que pasa.

```
$ aws ec2 get-console-output --instance i-f4f97f7f --region eu-west-1 --profile staging --query Output --output text
[...JSON...]
[  148.814016] cloud-init[2008]: --- END PUT BODY ---
[  148.814739] cloud-init[2008]: [2016-10-22T10:16:49+00:00] INFO: Chef Run complete in 9.238198883 seconds
[  148.895055] [sched_delayed] sched: RT throttling activated
[  148.920515] cloud-init[2008]: [2016-10-22T10:17:00+00:00] INFO: Running report handlers
[  148.920806] cloud-init[2008]: [2016-10-22T10:17:00+00:00] INFO: Report handlers complete
[  148.977162] cloud-init[2008]: chef-marketplace Reconfigured!
[  148.982350] cloud-init[2008]: Please wait while we set up Chef Server. This may take a few minutes to complete...
```

Pareció tardar un buen tiempo en iniciarse así que me pillé una ducha.

Al volver la salida no se había cambiado. Creo que no escribe cuando termine.

Bueno, en todos casos la guía no me manda entrar por SSH. Continuemos.

Need to create some directories and generate a private key to communicate with
the chef server.

```
$ mkdir learn-chef
$ cd learn-chef/
$ mkdir .chef
$ cd .chef
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/iainelder/.ssh/id_rsa): .
. already exists.
Overwrite (y/n)? n^C
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/iainelder/.ssh/id_rsa): ./id_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in ./id_rsa.
Your public key has been saved in ./id_rsa.pub.
The key fingerprint is:
SHA256:nZ5rduGr7hH6dJVi3qqKFoe5KyuYzo0ns1d7Ktoughc iainelder@Sistemass-MBP
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|                 |
|                 |
|         . .   . |
|       oS + o o  |
|  E  .+ .o =.+   |
|.o .. .+. =.o..  |
|*+*+..oo oo+o.   |
|oBX=o==..*B+o.   |
+----[SHA256]-----+
$ ls
id_rsa		id_rsa.pub
```

Ah, tengo que [configurar](https://docs.chef.io/aws_marketplace.html#configure-the-chef-server-title) primero el servidor Chef.

El enlace construido para apuntarme un nuevo usario sale así:
https://54.171.39.128/signup

Chrome no confia en el certificado de seguridad del servidor, pero sigamos tal
cual. Es un certificado autofirmado.

![Chef server signup dialog: "Please enter your instance ID to continue to the web interface."](assets/chef-server-e36c4.png)

Vaya.

!["Sign Up is disabled because it has been longer than 3600 seconds since Manage has been configured. Please run 'chef-manage-ctl reconfigure' to restart the timer."](assets/chef-server-baed6.png)


Vale, lancé otro servidor igual (usando "Launch more like this"). Otra vez el enlace:

https://54.154.232.242/signup

Esta vez va mejor. Pongo mi nombre, correo, y acepto los terminos.

![](assets/chef-server-95a7a.png)

![](assets/chef-server-071c3.png)

![](assets/chef-server-f3687.png)

![](assets/chef-server-b23ed.png)

![](assets/chef-server-e386e.png)

Okay, el equipo de inicio ya se ha bajado.

Por cierto, hay más salida en la consola de la instancia:

```
[  127.930067] cloud-init[1974]: Please wait while we set up Chef Server. This may take a few minutes to complete...
[  165.432872] cloud-init[1974]: [2016-10-22T17:39:23+00:00] INFO: Started chef-zero at chefzero://localhost:8889 with repository at /opt/opscode/embedded
[  165.435561] cloud-init[1974]: One version per cookbook
[  165.443359] cloud-init[1974]: [2016-10-22T17:39:23+00:00] INFO: *** Chef 12.12.19 ***
[  165.446092] cloud-init[1974]: [2016-10-22T17:39:23+00:00] INFO: Platform: x86_64-linux
[  165.446591] cloud-init[1974]: [2016-10-22T17:39:23+00:00] INFO: Chef-client pid: 2526
```

Hasta probar la conección todo va bien.

```
$ cd ~/Downloads
$ unzip chef-starter.zip
Archive:  chef-starter.zip
  inflating: chef-repo/README.md     
   creating: chef-repo/cookbooks/
   creating: chef-repo/cookbooks/starter/
   creating: chef-repo/cookbooks/starter/recipes/
  inflating: chef-repo/cookbooks/starter/recipes/default.rb  
   creating: chef-repo/cookbooks/starter/files/
   creating: chef-repo/cookbooks/starter/files/default/
  inflating: chef-repo/cookbooks/starter/files/default/sample.txt  
   creating: chef-repo/cookbooks/starter/attributes/
  inflating: chef-repo/cookbooks/starter/attributes/default.rb  
   creating: chef-repo/cookbooks/starter/templates/
   creating: chef-repo/cookbooks/starter/templates/default/
  inflating: chef-repo/cookbooks/starter/templates/default/sample.erb  
  inflating: chef-repo/cookbooks/starter/metadata.rb  
  inflating: chef-repo/cookbooks/chefignore  
  inflating: chef-repo/.gitignore    
   creating: chef-repo/.chef/
   creating: chef-repo/roles/
  inflating: chef-repo/.chef/knife.rb  
  inflating: chef-repo/roles/starter.rb  
  inflating: chef-repo/.chef/iainelder.pem  
$ cd chef-repo
$ knife ssl fetch
WARNING: Certificates from ec2-54-154-232-242.eu-west-1.compute.amazonaws.com will be fetched and placed in your trusted_cert
directory (/Users/iainelder/Downloads/chef-repo/.chef/trusted_certs).

Knife has no means to verify these are the correct certificates. You should
verify the authenticity of these certificates after downloading.

Adding certificate for ec2-54-154-232-242.eu-west-1.compute.amazonaws.com in /Users/iainelder/Downloads/chef-repo/.chef/trusted_certs/ec2-54-154-232-242_eu-west-1_compute_amazonaws_com.crt
$ knife client list
testorg-validator
```

Pero ¿qué hago para añadir máquinas virtuales al servidor Chef o para arrancar
nodos con el servidor Chef?

Bueno, parece que arrancar nodos es otra parte de la guía principal.
Continuemos usando el equipo de inicio como la referencia.

```
$ rm -rf ~/Bitbucket/learn-chef
$ rm ~/Downloads/chef-starter.zip
$ man unzip    
shell-init: error retrieving current directory: getcwd: cannot access parent directories: No such file or directory
chdir: error retrieving current directory: getcwd: cannot access parent directories: No such file or directory
$ unzip ~/Downloads/chef-starter.zip -d ~/Bitbucket/
Archive:  /Users/iainelder/Downloads/chef-starter.zip
  inflating: /Users/iainelder/Bitbucket/chef-repo/README.md  
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/recipes/
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/recipes/default.rb  
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/files/
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/files/default/
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/files/default/sample.txt  
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/attributes/
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/attributes/default.rb  
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/templates/
   creating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/templates/default/
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/templates/default/sample.erb  
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/starter/metadata.rb  
  inflating: /Users/iainelder/Bitbucket/chef-repo/cookbooks/chefignore  
  inflating: /Users/iainelder/Bitbucket/chef-repo/.gitignore  
   creating: /Users/iainelder/Bitbucket/chef-repo/.chef/
   creating: /Users/iainelder/Bitbucket/chef-repo/roles/
  inflating: /Users/iainelder/Bitbucket/chef-repo/.chef/knife.rb  
  inflating: /Users/iainelder/Bitbucket/chef-repo/roles/starter.rb  
  inflating: /Users/iainelder/Bitbucket/chef-repo/.chef/iainelder.pem  
$ cd ~/Bitbucket/chef-repo
$ git init
Initialized empty Git repository in /Users/iainelder/Bitbucket/chef-repo/.git/
$ git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.chef/
	.gitignore
	README.md
	cookbooks/
	roles/

nothing added to commit but untracked files present (use "git add" to track)
$ git add .
$ git commit --message "Iniciar con el equipo de inicio del servidor Chef."
[master (root-commit) 57e96c5] Iniciar con el equipo de inicio del servidor Chef.
 10 files changed, 226 insertions(+)
 create mode 100644 .chef/knife.rb
 create mode 100644 .gitignore
 create mode 100644 README.md
 create mode 100644 cookbooks/chefignore
 create mode 100644 cookbooks/starter/attributes/default.rb
 create mode 100644 cookbooks/starter/files/default/sample.txt
 create mode 100644 cookbooks/starter/metadata.rb
 create mode 100644 cookbooks/starter/recipes/default.rb
 create mode 100644 cookbooks/starter/templates/default/sample.erb
 create mode 100644 roles/starter.rb
```

Así es el contenido del archivo knife.rb:

```
$ cat .chef/knife.rb
# See https://docs.chef.io/aws_marketplace.html/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "iainelder"
client_key               "#{current_dir}/iainelder.pem"
chef_server_url          "https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg"
cookbook_path            ["#{current_dir}/../cookbooks"]
```

Sale de `ssl fetch` un aviso sobre la authencidad del certificados que no sale en la guía. ¿Qué significa?

```
$ knife ssl fetch
WARNING: Certificates from ec2-54-154-232-242.eu-west-1.compute.amazonaws.com will be fetched and placed in your trusted_cert
directory (/Users/iainelder/Bitbucket/chef-repo/.chef/trusted_certs).

Knife has no means to verify these are the correct certificates. You should
verify the authenticity of these certificates after downloading.

Adding certificate for ec2-54-154-232-242.eu-west-1.compute.amazonaws.com in /Users/iainelder/Bitbucket/chef-repo/.chef/trusted_certs/ec2-54-154-232-242_eu-west-1_compute_amazonaws_com.crt
$ knife ssl check
Connecting to host ec2-54-154-232-242.eu-west-1.compute.amazonaws.com:443
Successfully verified certificates from `ec2-54-154-232-242.eu-west-1.compute.amazonaws.com'
```

Vale, por fin parece que funciona. ¡Ahora a subir un cookbook!

Vaya, el cookbook `learn_chef_apache2` que escribí hace poco ya no existe. Se
desvaneció con el servidor EC2.

Venga, ¡en realidad sólo tengo que clonarlo desde un repositorio! El commit de
inicio se hizo hace 23 días. Qué moderno soy.

Lo clono usando el comando `submodule` porque ya está dentro de un repostitirio.

```
$ cd cookbooks/
$ git submodule add https://github.com/learn-chef/learn_chef_apache2 learn_chef_apache2
Cloning into '/Users/iainelder/Bitbucket/chef-repo/cookbooks/learn_chef_apache2'...
remote: Counting objects: 60, done.
remote: Total 60 (delta 0), reused 0 (delta 0), pack-reused 60
Unpacking objects: 100% (60/60), done.
```

¡Funciona!

```
$ knife cookbook upload learn_chef_apache2
Uploading learn_chef_apache2 [0.1.0]
Uploaded 1 cookbook.
$ knife cookbook list
learn_chef_apache2   0.1.0
```

Nos recomendan un nodo de Ubuntu 14.04 para que los resultados salgan igual de
los de la guía. Utilicé el [Amazon EC2 Image
Finder](https://cloud-images.ubuntu.com/locator/ec2/) para encontrar un imagen
apto, tipo `hvm:ebs-ssd`. Su AMI-ID es `ami-1c4a046f`.

![](assets/chef-server-c1304.png)

Lancé una instancia con la consola AWS su dirección IP es 54.194.108.80 y su
clave es la misma de la del servidor Chef.

Primero, probar SSH:

```
$ ssh -i ~/.ssh/ChefServer.pem ubuntu@54.194.108.80 echo "SSH funciona"
SSH funciona
```

Después de dos errores en el servidor Chef, el nodo se arranca. Parece que se instala el paquete de chef en el nodo antes de applicar el cookbook.

```
$ knife bootstrap 54.194.108.80 --ssh-user ubuntu --sudo --identity-file ~/.ssh/ChefServer.pem --node-name node1-ubuntu --run-list 'recipe[learn_chef_apache2]'
ERROR: Server returned error 503 for https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/nodes/node1-ubuntu, retrying 1/5 in 3s
ERROR: Server returned error 500 for https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/nodes/node1-ubuntu, retrying 2/5 in 8s
Creating new client for node1-ubuntu
Creating new node for node1-ubuntu
Connecting to 54.194.108.80
54.194.108.80 -----> Installing Chef Omnibus (-v 12)
54.194.108.80 downloading https://omnitruck-direct.chef.io/chef/install.sh
54.194.108.80   to file /tmp/install.sh.1543/install.sh
54.194.108.80 trying wget...
54.194.108.80 ubuntu 14.04 x86_64
54.194.108.80 Getting information for chef stable 12 for ubuntu...
54.194.108.80 downloading https://omnitruck-direct.chef.io/stable/chef/metadata?v=12&p=ubuntu&pv=14.04&m=x86_64
54.194.108.80   to file /tmp/install.sh.1547/metadata.txt
54.194.108.80 trying wget...
54.194.108.80 sha1	a2de7d933734d3a0c4b859576d0be472c5cd55f7
54.194.108.80 sha256	7073541beb4294c994d4035a49afcf06ab45b3b3933b98a65b8059b7591df6b8
54.194.108.80 url	https://packages.chef.io/files/stable/chef/12.15.19/ubuntu/14.04/chef_12.15.19-1_amd64.deb
54.194.108.80 version	12.15.19
54.194.108.80 downloaded metadata file looks valid...
54.194.108.80 downloading https://packages.chef.io/files/stable/chef/12.15.19/ubuntu/14.04/chef_12.15.19-1_amd64.deb
54.194.108.80   to file /tmp/install.sh.1547/chef_12.15.19-1_amd64.deb
54.194.108.80 trying wget...
54.194.108.80 Comparing checksum with sha256sum...
54.194.108.80 Installing chef 12
54.194.108.80 installing with dpkg...
54.194.108.80 Selecting previously unselected package chef.
(Reading database ... 51177 files and directories currently installed.)
54.194.108.80 Preparing to unpack .../chef_12.15.19-1_amd64.deb ...
54.194.108.80 Unpacking chef (12.15.19-1) ...
54.194.108.80 Setting up chef (12.15.19-1) ...
54.194.108.80 Thank you for installing Chef!
54.194.108.80 Starting the first Chef Client run...
54.194.108.80 Starting Chef Client, version 12.15.19
54.194.108.80 resolving cookbooks for run list: ["learn_chef_apache2"]
54.194.108.80 Synchronizing Cookbooks:
54.194.108.80   - learn_chef_apache2 (0.1.0)
54.194.108.80 Installing Cookbook Gems:
54.194.108.80 Compiling Cookbooks...
54.194.108.80 Converging 4 resources
54.194.108.80 Recipe: learn_chef_apache2::default
54.194.108.80   * apt_update[Update the apt cache daily] action periodic
54.194.108.80     - update new lists of packages
54.194.108.80     * directory[/var/lib/apt/periodic] action create (up to date)
54.194.108.80     * directory[/etc/apt/apt.conf.d] action create (up to date)
54.194.108.80     * file[/etc/apt/apt.conf.d/15update-stamp] action create_if_missing (up to date)
54.194.108.80     * execute[apt-get -q update] action run
54.194.108.80       - execute apt-get -q update
54.194.108.80   
54.194.108.80   * apt_package[apache2] action install
54.194.108.80     - install version 2.4.7-1ubuntu4.13 of package apache2
54.194.108.80   * service[apache2] action enable (up to date)
54.194.108.80   * service[apache2] action start (up to date)
54.194.108.80   * template[/var/www/html/index.html] action create
54.194.108.80     - update content in file /var/www/html/index.html from 538f31 to ef4ffd
54.194.108.80     --- /var/www/html/index.html	2016-10-22 23:31:20.685125999 +0000
54.194.108.80     +++ /var/www/html/.chef-index20161022-1631-5su8n7.html	2016-10-22 23:31:23.413125999 +0000
[...]
54.194.108.80 Running handlers:
54.194.108.80 Running handlers complete
54.194.108.80 Chef Client finished, 4/9 resources updated in 01 minutes 11 seconds
```

El nodo se ha enregistrado en el servidor Chef, los metadatos están disponibles
y, más importante, ¡el cookbook se ha aplicado bien!

```
$ knife node list
node1-ubuntu
$ knife node show node1-ubuntu
Node Name:   node1-ubuntu
Environment: _default
FQDN:        ip-172-31-44-173.eu-west-1.compute.internal
IP:          54.194.108.80
Run List:    recipe[learn_chef_apache2]
Roles:       
Recipes:     learn_chef_apache2, learn_chef_apache2::default
Platform:    ubuntu 14.04
Tags:        
$ curl 54.194.108.80
<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>
```

![](assets/chef-server-f2c48.png)

Luego vamos a actualizar el cookbook, subirlo al servidor Chef, y ver que los
cambios se apliquen en el nodo. Ya que actualizaremos el cookbook, sería mejor
si hiciera una bifurcación. Lo hicé usando la interfaz de Github y luego cambié
la copia local por mi bifurcación.

```
$ git remote rm origin
$ git remote add origin https://github.com/iainelder/learn_chef_apache2
$ git branch --set-upstream-to origin/master master
Branch master set up to track remote branch master from origin.
$ git fetch
$ git pull
Already up-to-date.
$ git branch add-metadata
$ git checkout add-metadata
Switched to branch 'add-metadata'
```

Actualicé la plantilla del indice y los metadatos. No pude subir las actualizaciones porque el servidor chef dejó de responder.

```
$ knife cookbook upload learn_chef_apache2
ERROR: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all, retry 1/5
ERROR: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all, retry 2/5
ERROR: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all, retry 3/5
ERROR: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all, retry 4/5
ERROR: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all, retry 5/5
ERROR: Network Error: Error connecting to https://ec2-54-154-232-242.eu-west-1.compute.amazonaws.com/organizations/testorg/cookbooks?num_versions=all - Operation timed out - connect(2) for "ec2-54-154-232-242.eu-west-1.compute.amazonaws.com" port 443
Check your knife configuration and network settings
```

La instancia reprobó su prueba de disponibilidad. Para llegar hasta aquí de nuevo:

* Lancé otra con la misma configuración antes de terminarla
* puse la identificación de la nueva instancia
* recreé mi usario
* bajé el equipo de inicio
* copié la clave SSH a la carpeta chef-repo
* actualicé la dirección del servidor Chef en `knife.rb`
* traé y comprobé el certificado SSL del servidor Chef
* cambié a la rama `master` del cookbook apache2
* arranqué el nodo de nuevo (ver abajo)
* cambié a la rama `add-metadata` del cookbook

Por cierto, el arranque detectó que ya existó una instalación de Chef en el nodo, y que todos los recursos ya habían sido actualizado.

```
$ knife bootstrap 54.194.108.80 --ssh-user ubuntu --sudo --identity-file ~/.ssh/ChefServer.pem --node-name node1-ubuntu --run-list 'recipe[learn_chef_apache2]'
Creating new client for node1-ubuntu
Creating new node for node1-ubuntu
Connecting to 54.194.108.80
54.194.108.80 -----> Existing Chef installation detected
54.194.108.80 Starting the first Chef Client run...
54.194.108.80 Starting Chef Client, version 12.15.19
54.194.108.80 resolving cookbooks for run list: ["learn_chef_apache2"]
54.194.108.80 Synchronizing Cookbooks:
54.194.108.80   - learn_chef_apache2 (0.1.0)
54.194.108.80 Installing Cookbook Gems:
54.194.108.80 Compiling Cookbooks...
54.194.108.80 Converging 4 resources
54.194.108.80 Recipe: learn_chef_apache2::default
54.194.108.80   * apt_update[Update the apt cache daily] action periodic (up to date)
54.194.108.80   * apt_package[apache2] action install (up to date)
54.194.108.80   * service[apache2] action enable (up to date)
54.194.108.80   * service[apache2] action start (up to date)
54.194.108.80   * template[/var/www/html/index.html] action create (up to date)
54.194.108.80
54.194.108.80 Running handlers:
54.194.108.80 Running handlers complete
54.194.108.80 Chef Client finished, 0/5 resources updated in 02 seconds
```

Vale, y ahora a actualizar.

```
$ knife cookbook upload learn_chef_apache2
Uploading learn_chef_apache2 [0.2.0]
Uploaded 1 cookbook.
$ knife ssh 54.194.108.80 'sudo chef-client' --manual-list --ssh-user ubuntu --identity-file ~/.ssh/ChefServer.pem
54.194.108.80 Starting Chef Client, version 12.15.19
54.194.108.80 resolving cookbooks for run list: ["learn_chef_apache2"]
54.194.108.80 Synchronizing Cookbooks:
54.194.108.80   - learn_chef_apache2 (0.2.0)
54.194.108.80 Installing Cookbook Gems:
54.194.108.80 Compiling Cookbooks...
54.194.108.80 Converging 4 resources
54.194.108.80 Recipe: learn_chef_apache2::default
54.194.108.80   * apt_update[Update the apt cache daily] action periodic (up to date)
54.194.108.80   * apt_package[apache2] action install (up to date)
54.194.108.80   * service[apache2] action enable (up to date)
54.194.108.80   * service[apache2] action start (up to date)
54.194.108.80   * template[/var/www/html/index.html] action create
54.194.108.80     - update content in file /var/www/html/index.html from ef4ffd to 70934d
54.194.108.80     --- /var/www/html/index.html	2016-10-22 23:31:23.413125999 +0000
54.194.108.80     +++ /var/www/html/.chef-index20161023-3606-1dwjnob.html	2016-10-23 02:48:35.825125999 +0000
54.194.108.80     @@ -1,6 +1,6 @@
54.194.108.80      <html>
54.194.108.80        <body>
54.194.108.80     -    <h1>hello world</h1>
54.194.108.80     +    <h1>hello from ip-172-31-44-173.eu-west-1.compute.internal</h1>
54.194.108.80        </body>
54.194.108.80      </html>
54.194.108.80
54.194.108.80 Running handlers:
54.194.108.80 Running handlers complete
54.194.108.80 Chef Client finished, 1/5 resources updated in 01 seconds
```

Lo interesante aquí es que no es el servidor que manda la actuación sino mi
portátil. Básicamente ejecutamos chef-client por SSH. Pero fíjense en el consejo en la guía:

> Remember, in practice it's common to configure Chef to act as a service that
> runs periodically or as part of a continuous integration or continuous
> delivery (CI/CD) pipeline. For now, we're updating our server configuration by
> running chef-client manually.

Es decir que seguramente nos enseñerá la manera más rentable de hacerlo.

¡Pero mira como funciona!

```
$ curl 54.194.108.80
<html>
  <body>
    <h1>hello from ip-172-31-44-173.eu-west-1.compute.internal</h1>
  </body>
</html>
```

![Chrome desplegando el indice: "hello from ip-172-31-44-173.eu-west-1.compute.internal"](assets/chef-server-35775.png)

# [Resolve a failed chef-client run](https://learn.chef.io/tutorials/manage-a-node/ubuntu/aws/resolve-a-failure/)

Ahora vamos a romperlo a propósito para aprender resolver el fallo.

Tras confirmar los cambios en la rama del cookbook, subé el cookbook y lanzé la actualización.

```
$ knife cookbook upload learn_chef_apache2
Uploading learn_chef_apache2 [0.3.0]
Uploaded 1 cookbook.
$ knife ssh 54.194.108.80 'sudo chef-client' --manual-list --ssh-user ubuntu --identity-file ~/.ssh/ChefServer.pem
54.194.108.80 Starting Chef Client, version 12.15.19
54.194.108.80 resolving cookbooks for run list: ["learn_chef_apache2"]
54.194.108.80 Synchronizing Cookbooks:
54.194.108.80   - learn_chef_apache2 (0.3.0)
54.194.108.80 Installing Cookbook Gems:
54.194.108.80 Compiling Cookbooks...
54.194.108.80 Converging 4 resources
54.194.108.80 Recipe: learn_chef_apache2::default
54.194.108.80   * apt_update[Update the apt cache daily] action periodic (up to date)
54.194.108.80   * apt_package[apache2] action install (up to date)
54.194.108.80   * service[apache2] action enable (up to date)
54.194.108.80   * service[apache2] action start (up to date)
54.194.108.80   * template[/var/www/html/index.html] action create
54.194.108.80     * cannot determine user id for 'web_admin', does the user exist on this system?
54.194.108.80     ================================================================================
54.194.108.80     Error executing action `create` on resource 'template[/var/www/html/index.html]'
54.194.108.80     ================================================================================
54.194.108.80     
54.194.108.80     Chef::Exceptions::UserIDNotFound
54.194.108.80     --------------------------------
54.194.108.80     cannot determine user id for 'web_admin', does the user exist on this system?
54.194.108.80     
54.194.108.80     Resource Declaration:
54.194.108.80     ---------------------
54.194.108.80     # In /var/chef/cache/cookbooks/learn_chef_apache2/recipes/default.rb
54.194.108.80     
54.194.108.80      18: template '/var/www/html/index.html' do
54.194.108.80      19:   source 'index.html.erb'
54.194.108.80      20:   mode '0644'
54.194.108.80      21:   owner 'web_admin'
54.194.108.80      22:   group 'web_admin'
54.194.108.80      23: end
54.194.108.80     
54.194.108.80     Compiled Resource:
54.194.108.80     ------------------
54.194.108.80     # Declared in /var/chef/cache/cookbooks/learn_chef_apache2/recipes/default.rb:18:in `from_file'
54.194.108.80     
54.194.108.80     template("/var/www/html/index.html") do
54.194.108.80       action [:create]
54.194.108.80       retries 0
54.194.108.80       retry_delay 2
54.194.108.80       default_guard_interpreter :default
54.194.108.80       source "index.html.erb"
54.194.108.80       declared_type :template
54.194.108.80       cookbook_name "learn_chef_apache2"
54.194.108.80       recipe_name "default"
54.194.108.80       mode "0644"
54.194.108.80       owner "web_admin"
54.194.108.80       group "web_admin"
54.194.108.80       path "/var/www/html/index.html"
54.194.108.80     end
54.194.108.80     
54.194.108.80     Platform:
54.194.108.80     ---------
54.194.108.80     x86_64-linux
54.194.108.80     
54.194.108.80
54.194.108.80 Running handlers:
54.194.108.80 [2016-10-23T04:10:37+00:00] ERROR: Running exception handlers
54.194.108.80 Running handlers complete
54.194.108.80 [2016-10-23T04:10:37+00:00] ERROR: Exception handlers complete
54.194.108.80 Chef Client failed. 0 resources updated in 01 seconds
54.194.108.80 [2016-10-23T04:10:37+00:00] FATAL: Stacktrace dumped to /var/chef/cache/chef-stacktrace.out
54.194.108.80 [2016-10-23T04:10:37+00:00] FATAL: Please provide the contents of the stacktrace.out file if you file a bug report
54.194.108.80 [2016-10-23T04:10:37+00:00] ERROR: template[/var/www/html/index.html] (learn_chef_apache2::default line 18) had an error: Chef::Exceptions::UserIDNotFound: cannot determine user id for 'web_admin', does the user exist on this system?
54.194.108.80 [2016-10-23T04:10:37+00:00] FATAL: Chef::Exceptions::ChildConvergeError: Chef run process exited unsuccessfully (exit code 1)
```

Vaya, parece que el usario web_admin aún no existe.

```
54.194.108.80     * cannot determine user id for 'web_admin', does the user exist on this system?
```

Por cierto, según [la documentación](https://docs.chef.io/resource_user.html) `system true` significa que `useradd` vaya a crear una cuenta de sistema.

```
-r, --system
    Create a system account.

    System users will be created with no aging information in /etc/shadow, and
    their numeric identifiers are chosen in the SYS_UID_MIN-SYS_UID_MAX range,
    defined in /etc/login.defs, instead of UID_MIN-UID_MAX (and their GID
    counterparts for the creation of groups).

    Note that useradd will not create a home directory for such an user,
    regardless of the default setting in /etc/login.defs (CREATE_HOME). You
    have to specify the -m options if you want a home directory for a system
    account to be created.
```

Tras confirmar los arreglos, lancé la actualizatión de neuvo. Esta vez salió bien.

```
$ knife cookbook upload learn_chef_apache2
Uploading learn_chef_apache2 [0.3.1]
Uploaded 1 cookbook.
$ knife ssh 54.194.108.80 'sudo chef-client' --manual-list --ssh-user ubuntu --identity-file ~/.ssh/ChefServer.pem
54.194.108.80 Starting Chef Client, version 12.15.19
54.194.108.80 resolving cookbooks for run list: ["learn_chef_apache2"]
54.194.108.80 Synchronizing Cookbooks:
54.194.108.80   - learn_chef_apache2 (0.3.1)
54.194.108.80 Installing Cookbook Gems:
54.194.108.80 Compiling Cookbooks...
54.194.108.80 Converging 6 resources
54.194.108.80 Recipe: learn_chef_apache2::default
54.194.108.80   * apt_update[Update the apt cache daily] action periodic (up to date)
54.194.108.80   * apt_package[apache2] action install (up to date)
54.194.108.80   * service[apache2] action enable (up to date)
54.194.108.80   * service[apache2] action start (up to date)
54.194.108.80   * group[web_admin] action create
54.194.108.80     - create group web_admin
54.194.108.80   * linux_user[web_admin] action create
54.194.108.80     - create user web_admin
54.194.108.80   * template[/var/www/html/index.html] action create
54.194.108.80     - change owner from 'root' to 'web_admin'
54.194.108.80     - change group from 'root' to 'web_admin'
54.194.108.80
54.194.108.80 Running handlers:
54.194.108.80 Running handlers complete
54.194.108.80 Chef Client finished, 3/7 resources updated in 01 seconds
$ curl 54.194.108.80
<html>
  <body>
    <h1>hello from ip-172-31-44-173.eu-west-1.compute.internal</h1>
  </body>
</html>
```

Otra cosita más antés que terminemos. Ahora ya se ha acabado el nodo, y deberíamos borrar todo.

```
$ knife node delete node1-ubuntu
Do you really want to delete node1-ubuntu? (Y/N) Y
Deleted node[node1-ubuntu]
$ knife client delete node1-ubuntu
Do you really want to delete node1-ubuntu? (Y/N) Y
Deleted client[node1-ubuntu]
$ knife cookbook delete learn_chef_apache2
Which version(s) do you want to delete?
1. learn_chef_apache2 0.3.1
2. learn_chef_apache2 0.3.0
3. learn_chef_apache2 0.2.0
4. learn_chef_apache2 0.1.0
5. All versions

5
Deleted cookbook[learn_chef_apache2][0.3.1]
Deleted cookbook[learn_chef_apache2][0.3.0]
Deleted cookbook[learn_chef_apache2][0.2.0]
Deleted cookbook[learn_chef_apache2][0.1.0]
$ ssh -i ~/.ssh/ChefServer.pem ubuntu@54.194.108.80
Welcome to Ubuntu 14.04.5 LTS (GNU/Linux 3.13.0-100-generic x86_64)

 * Documentation:  https://help.ubuntu.com/

  System information as of Sun Oct 23 04:42:36 UTC 2016

  System load:  0.0               Processes:           101
  Usage of /:   12.9% of 7.74GB   Users logged in:     0
  Memory usage: 10%               IP address for eth0: 172.31.44.173
  Swap usage:   0%

  Graph this data and manage this system at:
    https://landscape.canonical.com/

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

New release '16.04.1 LTS' available.
Run 'do-release-upgrade' to upgrade to it.


Last login: Sun Oct 23 04:42:36 2016 from 250.166.14.37.dynamic.jazztel.es
ubuntu@ip-172-31-44-173:~$ sudo rm /etc/chef/client.
client.pem  client.rb   
ubuntu@ip-172-31-44-173:~$ sudo rm /etc/chef/client.pem
```
