Descubré la librería Ridley por este error:

```
Ridley::Errors::MissingNameAttribute: The metadata at '/Users/iainelder/Repos/Wakoopa/chef/cookbooks/newrelic' does not contain a 'name' attribute. While Chef does not strictly enforce this requirement, Ridley cannot continue without a valid metadata 'name' entry.] on default-ubuntu-1404
```

https://github.com/berkshelf/ridley

Aprendé que en ruby `continue` se llama `next` y tiene forma idiomática:

```
.each do |x|
  next if x == y
  puts x
end
```

```
find .. -mindepth 1 -maxdepth 1 -type d -not -exec test -e "{}/metadata.rb" \; -print
```

Para añadir el mínimo de metadata.rb en el repo de Chef:

```
find . -mindepth 1 -maxdepth 1 -type d \
-not -exec test -e "{}/metadata.rb" \; \
-exec sh -c 'echo name '"\'"'$(basename {})'"\'"' > {}/metadata.rb' \;
```

Aprendé como provar si algo está en una lista o no:

```
list.include?(elem)
```
