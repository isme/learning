# Gestionar una aplicación web

https://learn.chef.io/manage-a-web-app/

Generé una recetera base y la convergí en Ubuntu 14.04. Descubrí que el comando `kitchen converge` implica `kitchen create`. Es decir, la máquina se crea antes de la convergencia si aún no existe.

Utilizé una tercera recetera [`apt`](https://supermarket.chef.io/cookbooks/apt) desde el Supermarcado Chef; declaré una limitación en su versión en los metadatos de mi recetera con esta linea: `depends 'apt', '~> 4.0'`. Comprobé los detalles en el supermercado con `knife supermarket show apt`. Despues de convergir, verifiqué que existiera el señal `/var/lib/apt/periodic/update-success-stamp`, y con el comando `stat -c %y ...` que su fecha de creación fuera reciente.

Usé el recurso `firewall_rule` de la recetera [`firewall`](https://supermarket.chef.io/cookbooks/firewall) del supermarcado. Crée el archivo de atributos para mi recetera envoltura con `chef generate attribute`. Superé en ese archivo los valores predeterminados de la recetera por redefinir la clave `default['firewall']['allow_ssh']` mejor que `override[...]`. Comprobé el resultado con el comando `sudo ufw status`.

Creé un usario y grupo con los recursos integrales `user` y `group`. Comprobé el resultado con los comandos `getent passwd web_admin` y `getent group web_admin`.

Usé los recursos `httpd_service` y `httpd_config` de la recetera [`httpd`](https://supermarket.chef.io/cookbooks/httpd) y puse un archivo de configuración de apache para lanzar el esquéleto de la web usando un servicio por host. Comprobé el resultado con los comandos `stat -c "%A (%a) %U %G" /...`, `sudo service apache2-customers status`.

Declaré una función en los atributos para generar contraseñas aleatorias y seguras. Puse los valores en el `normal_unless`para que se cambie sólo la primera vez. Añadé las contraseñas de prueba al archivo .kitchen.yml.

Usé los recursos `mysql_client` y `mysql_service` de la receta [`mysql`](https://supermarket.chef.io/cookbooks/mysql) para instalar MySQL. Comprobé que mysql etstuviera escuchando con `netstat -tap` y q pudiera conectarme a los bases de datos predeterminados con `mysqlshow`.

Usé los recursos `mysql2_chef_gem` de la recetera [mysql2_chef_gem](https://supermarket.chef.io/cookbooks/mysql2_chef_gem), `mysql_database`, y `mysql_database_user` de la recetera [database](https://supermarket.chef.io/cookbooks/database), todos para crear una base de datos MySQL con un usario. Usé `cookbook_file` y `execute` para crear el esquema y poner datos. Para comporobar el usario leí la tabla `mysql.user`, para comprobar los permisos usé `show grants`, y para leer los datos, seleccioné de la tabla de la app.

Instalé PHP en Apache con los recursos `httpd_module` y `package`. Desplegué la app con el recurso `template`. Ecribí PHP y depuré los fallos por copiar. Comprobé que la recetera funcionara en una nueva instancia por destruir y reconvergirla.

Pendiente: Ejecutar su applicación web en un nodo. Aprederé Berkshelf.

Pendiente: Revisar limitaciónes de versiones http://docs.chef.io/cookbook_versions.html

Pendiente: Leer de los atribuitos automáticos https://docs.chef.io/ohai.html#automatic-attributes

Pendiente: Leer del nivel de precedencia de normal y cuando se usa https://docs.chef.io/attributes.html#use-attribute-files

Pendiente: Leer de las características de búsqueda en Chef. https://docs.chef.io/chef_search.html

Pendiente: Leer de chef-vault https://github.com/Nordstrom/chef-vault
