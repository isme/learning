# Videos

## [What happens during Knife bootstrap](https://learn.chef.io/skills/beyond-essentials-1/)

Lo que pasa durante el arranque «Knife»

Qué pasa realmente cuando ejecutamos el comando `knife bootstrap`?

El flujo habitual de trabajo es desde tu terminal conectarte al servidor de Chef
para subir libros de recetas y listas de ejecución. A la vez los nodos que ya
están arrancados se reportan al servidor periodicamente por el programa cliente.
El terminal y el nodo realmente nunca se comunican en este flujo.

El proceso de arranque es una excepción; es decir, para arrancar el nodo nos
conectamos directamante para inicializar el nodo y copiar archivos. Los dos
protocols principales son SSH, para hacer el login, y el SCP para copiar
archivos. Es importante porque el nodo al pricipio no tiene ni idea del servidor
de Chef. Asimismo el arranque consigue dos cosas fundamentales:

* que el nodo sepa donde se encuentra el servidor (le mandamos su dirección)
* que el nodo pueda autentificarse con el servidor (le damos una clave)

Luego incluso el arranque termina con la ejecución inaugural del cliente de
Chef. Esto ocurre en tres pasos:

* instalar el programa cliente de Chef (bajando un paquete .deb en Ubuntu)
* configurar el cliente para comunicarse con el cliente (con los datos ya copiados)
* ejecutar el cliente (esta parte ocurre periodicamente desde el arranque)

Tanto como el nodo no sabía del servidor, el servidor tampoco tenía ningún
conocimiento del nodo. Por eso la primera ejecución es importante, porque es la
primera vez que el nodo manda sus detalles al servidor.

El servidor guarda los detalles en una base de datos, actualmente PostgreSQL, y
luego usando Solr hace un indice para facilitar acceso rápido de los datos de
nodo. Vale saber que haya dos planos  porque hay una pequeña demora de unos
cuantos segundos para retener los datos en el indice.

Pero qué se instala realmente en el nodo? Utiliza [el instalador omnibus de
Ruby](https://github.com/chef/omnibus) para poner un entorno de Ruby completo y
aislado de cualquier otro que esté ya en el sistema. También pone las
herramientas básicas como el programa cliente `chef-client` y analizador `ohai`.
Sin embargo, no pone todas las herramientas del [equipo de desarrollo de
Chef](https://github.com/chef/chef-dk), para que no se use como estación de
trabajo.

Unas carpetas importantes son:

* `/etc/chef` - configuración del cliente y la clave del servidor
* `/var/chef/backup` - copias de seguridad de archivos afectados por ejecucion
* `/var/chef/cache` - caché de las recetas y la salida del proceso
* `/opt/chefdk` - donde se guardan las herramientas del equipo de desarrollo

En realidad hay muchos pasos para arrancar el nodo, pero todo lo complejo está
bajo control de knife así que no hay por que preocuparte o buscar otra solución
propia para el arranque.
