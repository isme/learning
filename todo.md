
TODO:

* http://blog.mailgun.com/25-465-587-what-port-should-i-use/
* openssl s_client -servername goomba.staging.wakoopa.com -connect goomba.staging.wakoopa.com:443 | openssl x509 -text
* Study ssh to ec2 instance by tag, generate ssh_config
* Read this for Adrián: https://medium.freecodecamp.com/lessons-from-my-post-bootcamp-job-search-in-london-cb37ea12ec2f#.ajidpjvkg
* Learn the Seth Vargo's [Berkshelf workflow](https://sethvargo.com/berkshelf-workflow/).
* Finish reading https://medium.freecodecamp.com/a-study-plan-to-cure-javascript-fatigue-8ad3a54f2eb1#.jix529rbg
* Test driven infrastructure with chef http://shop.oreilly.com/product/0636920030973.do
* Cloud-init: fix the systemd mount bug.
* Check out spanish development videos https://www.genbetadev.com/formacion/otros-12-canales-mas-de-youtube-de-desarrollo-en-espanol-que-no-te-puedes-perder?utm_source=genbetadev&utm_medium=network&utm_campaign=no_te_pierdas
* Document the process of ripping youtube talks to audio so I can listen to them on my way to work.
* Get noise-cancelling headphones.
* Figure out how to get Terminal, git, vim, and Bitbucket to all play nice with UTF-8 characters.
* Do all of the above while avoiding complaints from Ubuntu like "WARNING! Your environment specifies an invalid locale."
* Watch: [What happens during a Chef run](https://www.youtube.com/watch?v=egzmSVVP0rg&index=2&list=PL11cZfNdwNyNciM-PmIrO0hkSZB-ir52t)
* Watch: [What is the security model used by Chef client](https://www.youtube.com/watch?v=egzmSVVP0rg&index=2&list=PL11cZfNdwNyNciM-PmIrO0hkSZB-ir52t)
* Watch [Attribute precedence explained](https://www.youtube.com/watch?v=egzmSVVP0rg&index=2&list=PL11cZfNdwNyNciM-PmIrO0hkSZB-ir52t)
* https://www.packer.io/intro/why.html
* Inspect the content of the folders created during the Chef bootstrap.
* Where does the 98-server-info 99-zzz-admin convention come from?
* Read about DNS (Dummies Guide to, DNS and BIND: Help for System Administrators)
* Continue with the Chef training.
* Watch Matz's keynote from EuRuKo https://www.youtube.com/watch?v=8aHmArEq4y0
* Set up a personal website using AWS. Use it to document the next projects.
* Figure out how to commit to bitbucket from my mac.
* Get a second monitor: Dell UP2516D or something less expensive.
* How you see the final result of chef template file without actually running the cookbook on a node? The template references a value in the node hash.
* How do you get the name of an existing AWS resource like an IAM role and write it into an application configuration file using chef?
* Figure out how to accept the IP ranges of AWS EC2 instances using SSH config.
* Explain how SSH works to a non-techy friend.
* Encrypt something using my public key and decrypt it using my private key.
* Explore checksums and their applications (md5, sha 256, etc)
* Perform a man-in-the-middle attack on an SSH connection.
* Learn how to check EC2 keypairs (see evernote)
* How is Ubuntu's ssh "Last login" message generated?

        Last login: Thu Sep 29 16:09:06 2016 from 095-097-134-250.static.chello.nl

* Learn more about SSH in general (specific book? Unix Power tools?)
* http://security.stackexchange.com/questions/39990/is-it-safe-to-disable-ssh-host-key-checking-if-key-based-authentication-is-used
* Try [new CloudFormation features](https://aws.amazon.com/blogs/aws/aws-cloudformation-update-yaml-cross-stack-references-simplified-substitution/): YAML, cross stack references, and simplified substitution.
* Use OpsWorks to deploy an existing Chef cookbook.
* Use Codeship to deploy a ruby project from Github to AWS.
* Use Docker to develop a ruby application.
* Speed up multi-region AWS queries with parallel ruby.
* Reimplement my Notepad++ macros in vim.
* Finish reading Learn Ruby the Hard Way.
* Are "launch-wizard-" security groups automatically deleted?
* Read another Ruby book
* Read a git book: Pro Git, Git from the Bottom Up, Jump Start Git.
* Finish reading Seven Databases in Seven Weeks.
* Get a Linux qualification.
* Get an AWS qualification.
* Review the Trello list of learning. What do you still want to learn?
* Learn about monit.
* Learn about ngninx. https://www.amazon.com/dp/1849510865/?tag=midl2103-20
* how password expiration influences password choice (Passwords Con)
* https://blog.chef.io/2013/12/03/doing-wrapper-cookbooks-right/
* http://engineering.ooyala.com/blog/stop-bashing-tarballs-chef
* http://sysadvent.blogspot.com.es/2012/12/day-24-twelve-things-you-didnt-know.html
* http://programmingisterrible.com/post/139222674273/write-code-that-is-easy-to-delete-not-easy-to
* Add a Brewfile to dotfiles
* https://dotfiles.github.io/
* Can I add my AWS Console config to dotfiles? EC2 columns at least
* Fíjate en RetroPie Emulador
* http://chris.beams.io/posts/git-commit/
* https://www.michaelwlucas.com/tools/ssh
* JSON parsing is a minefield. http://www.kora.li/admin.html#/index/p?u=nst021&s=JSON&c=softshake&e=Donkey_Kong http://seriot.ch/parsing_json.html
* [What is a non-RFC 1918 address range?](https://aws.amazon.com/about-aws/whats-new/2016/10/announcing-ec2-dns-support-for-non-rfc-1918-address-ranges/)
* Official study guide for amazon solutions architect associate https://www.amazon.com/Certified-Solutions-Architect-Official-Study-ebook/dp/B01M6W6WYD/ref=mt_kindle?_encoding=UTF8&me=&tag=midl2103-20
* http://keepachangelog.com/en/0.3.0/
* http://atombrenner.blogspot.com.es/2016/03/yaml-cloudformation-templates.html
* https://clusterhq.com/
* https://airbladesoftware.com/notes/three-chef-gotchas/
* branch.io
* the 12 principles of animation
* CSS Grid
* [PluralSight Chef Courses](https://www.pluralsight.com/search?q=chef&clm_id=581c999f511e0f0e00db2b15&CLM_Id__c=581c999f511e0f0e00db2b15&categories=all)
* http://www.b-eye-network.com/view/17223
* http://releaseengineer.blogspot.com.es/
* https://github.com/colinbjohnson/aws-missing-tools
* http://www.spinnaker.io/
* Can squid log different attributes to different files? How do you join them again?
* Mixcloud downloader http://www.mixcloud-downloader.com/
* Terraform to setup a mix of EC2 and non-EC2 instances
* Maxwell's Demon http://maxwells-daemon.io/
* Check out how change detection works https://www.changedetection.com/
* compare xray and sentry
* move a password from the repo to EC2 sytems manager
* finish the static blog set up!
* how do you get your public IP address without asking canihazip or similar service?