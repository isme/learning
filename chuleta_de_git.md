# Chuleta de Git

Mostrar historia con dif:

```
git log --patch <path>
```

Añadir pedazos de un nuevo archivo:

```
git add --intent-to-add <nuevo_archivo>
git add --patch
Stage this hunk?: e
```

No se hace directamente `add --patch` al nuevo archivo porque git dice sólo
`No changes` (sin cambios).

Añadir todos cambios a los archivos ya en git (ignorar archivos no seguidos)

```
git add --update
```

Ver el registro de otra rama:

```
git log --branches <rama>
```

La opción `--branch` en singular no existe.


Ver todos cambios en una rama y no en la otra.

```
git log development...master --not development
```

Entresacar un cambio que es fusión de dos padres. El padre correcto suele ser 1.

```
git cherry-pick 1234567890abcdef1234567890abcdef12345678 --mainline 1
```

Comprobar el padre para entresacar. Fíjese en la salida. La linea `Merge: ` es
la lista de padres. El padre correcto es el de la rama de characterística, no
del principal. En este ejemplo el padre correcto es 1.


```
git log --branches <rama_de_characterística>
[...]
commit 1234567890abcdef1234567890abcdef12345678
Merge: acde3b0 fedcba0
Author: Iain Elder <iainelder@hotmail.co.uk>
Date:   Wed Nov 2 12:00:56 2016 +0100

    Merge pull request #162 from org/feature_branch

    ABC-123 Add new feature

commit fedcba09876543210fedcba09876543210fedcba
[...]
```

Crear y saltar a una nueva rama en un comando:

```
git checkout -b <nueva_rama>
```

Contar todos los cambios desde ramificación:

```
git rev-list --count HEAD ^rama_de_base
```

Ver cuales archivos sin seguimiento sería quitado:

```
git clean --force --dry-run
```

Buscar ramas en un conjunto de repos:

```
find . -mindepth 1 -maxdepth 1 -type d \
-exec sh -c 'echo {}; git -C {} branch -r | grep -E "development|master"' \;
```

Actualizar la fecha después de hacer un rebase:

```
git rebase --interactive <rama_de_base>
git commit --amend --no-edit --date "now"
```
