2016-10-13 Thursday

* [Learned the basics of chef](https://learn.chef.io/tutorials/learn-the-basics)
* [Chef server on AWS Marketplace](https://aws.amazon.com/marketplace/pp/B01AMIH01Q)

2016-10-19 Tuesday

* Completed up to [Learn Ruby the Hard Way Exercise 24](https://learnrubythehardway.org/book/ex24.html).

2016-10-23 Saturday

* [Managed a node](https://learn.chef.io/tutorials/manage-a-node/) using Chef server. Set up Chef server using Amazon Marketplace, configured my development environment, used knife to connect to the chef server, upload cookbooks, and run the cookbooks on a node. See detailed notes.

2016-10-24 Thursday

* Watched a Chef training video to learn about the internals of the bootstrap
process. Now I can explain why the bootstrap is necessary and how it differs
from the normal workflow. See detailed notes in Spanish.

2016-10-25 Wednesday

* Watched part of Sudo: You're doing it wrong to learn more about the sudo tool. taking notes in Spanish aunque sea en inglés. Got a reference to the "Hail Mary Cloud".

2016-10-27 Thursday

* Watched a bit more of the same video.

2016-10-28 Friday

* Figured out how to rip youtube talks to audio I can listen to on my way to work.
* Started the read about the "[hail mary cloud](http://bsdly.blogspot.com.es/2013/10/the-hail-mary-cloud-and-lessons-learned.html)".

2016-10-30 Sunday

* Got some more technical vocabulary in spanish.


2016-10-31 Monday

* Continued to read about the "hail mary cloud". In the comments I learned about [DenyHosts](http://denyhosts.sourceforge.net/) for collaborative abuse blocking.
* Finshed watching "Sudo: You're Doing it Wrong".

2016-11-01 Tuesday

* First draft of the process of ripping youtube talks.
* Learned about [Mortgage Driven Development](http://codemanship.co.uk/parlezuml/blog/?postid=147)
* [Don't shout at JBODs](https://www.youtube.com/watch?v=tDacjrSCeq4). But if you do, use Fishworks dtrace to figure out exactly which disk and how much latency.
* Completed the Local Development tutorial for Chef. Now I know how to use kitchen to test my cookbooks in multiple virtual machines running different versions of Ubuntu.
* Started the Chef [Web Application Management](https://learn.chef.io/manage-a-web-app/ubuntu/configure-the-database/) tutorial. Learned how to reuse community cookbooks and wrap them to change their behavior.

2016-11-02 Wednesday

* Continued the Chef Web Application management tutorial. Learned a lot of third-party modules for installing common components.
* Attended Sudoers. Met the local sysadmin community. Heard one talk on Hiera and Puppet and another on Conda.

2016-11-09 Wednesday

* Attended AWS Webinar on OpsWorks. Learned about how it supports continuous delivery and continuous integration using chef. See detailed notes.

2016-11-12 Saturday

* Adapted Berkshelf to work with a monolithic chef repository to work around non-transitive Berksfiles (See [seth vargo's post](https://sethvargo.com/berksfile-magic/))
* Learned various things along the way (see Notas_de_kitchen.md)

2016-11-13 Tuesday

* Wrote unit tests for chef using BATS, Bash Automatic Testing System
* Learned that git has a [daemon process](https://git-scm.com/book/tr/v2/Git-on-the-Server-Git-Daemon) to run a simple git server
* Learned that Ubuntu 10.04 basically doesn't work in a VM any more.
* Learned that kitchen supports ERB templates
* Introduced to [runit init system replacement](http://smarden.org/runit/)
* Learned to use serverspec for testing chef.
* Introduction to [Leibniz](https://github.com/Atalanta/leibniz), integration testing using Chef

2016-11-16 Wednesday

* Attended Mapathon. Validated proyecto 2256 tarea 188.
* Learned to use jOSM again to validate changes in HOT.

2016-11-26 Saturday

* Learned the basic Packer workflow.
* Contributed my first [pull request](https://github.com/mitchellh/packer/pull/4207) to an open source project on Github. The PR adds a note about more secure ways to use AWS credentials.

2017-01-31 Tueday

* Christmas fug over.
* Attended AWS webinar "Auto escalado con EC2". See notes.

2017-02-08 Wednesday

* Explored cfn-config as a replacement for Wakoopa's custom stack deployment tool. Really promising, but needs some adjustments to integrate with our environment. Will contact the maintainer with my thoughts.

2017-02-09 Thursday

* Experimented with squid in a docker container. Squid is available as a package on alpine linux.

2017-02-10 Friday

* Community engagement: Published a repro for a bug in Amazon CloudFormation "Internal failure on removing ES domain access policy" https://forums.aws.amazon.com/thread.jspa?messageID=766563

2017-02-11 Saturday

* Started to write a web scraper in Scrapy for the gig website. Chose freedonia as the first source because all the data is one page.

2017-02-12 Sunday

* Learned how to capture HTTP sessions with Betamax to reduce the load on data sources during testing.

2017-03-28 Monday

* Learned how to install a Debian package from the source repository. https://wiki.debian.org/BuildingTutorial

2018-02-06 Tuesday

* Sat the mock exam for the AWS Associate Solutions Architect certification designed by Simplilearn. Got just over a 70% pass rate. They explained the result for each question, so I'll revise it tomorrow.

2018-02-07 Wednesday

* Reviewed the results of the exam and made a list of every that I have to revise. See the notes in the wakoopa-notes repo called "AWS-Assocaite Solution Architect exam notes". (yes, i misspelled it).
* Learned that Amazon S3 reduced reduncancy storage is actually more expensive than standard access. I thought it might be a mistake, so I checked and found out it's true, because the reduced redundancy storage option is being deprecated. This blog confirms it: https://mysteriouscode.io/blog/aws-s3-storage-classes-pricing-is-not-what-you-think/
* Signed up for the Mysterious Code blog updates. It's a UK-based AWS consultancy. Should be worth reading to get an idea of what the pros do.
* Learned that you can require MFA to delete things from S3. Documented here: https://docs.aws.amazon.com/es_es/AmazonS3/latest/dev/Versioning.html#MultiFactorAuthenticationDelete

2018-02-08 Thursday

* Passed the AWS Associate Solutions Architect. I'm now certified. Updated my linked in profile with the new qualification. Struggled with questions about VPC, ECS, and IAM federation, and EBS encryption, and what can trigger lambdas.

2018-02-09 Friday

* Discovered the "Open in Github" and "Confluence Markup" plugins for VS Code. Speeding up my workflow!

2018-02-...

* Lerned to use the elasticsearch API to manage snapshots.
* Learned to use S3 request and data metrics while investingating performance issues with the aws snapshot storage plugin for elasticsearch

2018-02-21 Tuesday

* Learned a lot about MySQL query optimization when I came across a query that ran fast on its own but horribly slow when as a subquery. Discovered that the wrong column can cause the 5.5 engine to slow to a crawl. The 5.7 engine can handle it, though. I'm writing a blog post about this one.
* Learned how to generate a tally table and a calendar table for MySQL.
* Discovered "Use the index, Luke!" website by Markus Winand. I think I remember this blog from my SQL Server days. It has great examples of how to generate a tally table in MySQL. He has a book called "SQL Performance Explained"...

2018-02-22 Wednesday

* Learned how to use R and ggplot to generate a time series plot. Thanks Bernat Huget.

2018-02-23 Thursday

* Learned a nicer way to fix the dates on git rebases. `git commit --amend --no-edit --date="now"` Perfect!

2018-02-25 Saturday

* Revised how to compile and patch a Debian package. Discovered that squid MAXTCPLISTPORTS constant can be set at compile time. Learned to use quilt to patch the Makefile to build the package with just 1 port for testing. https://github.com/squid-cache/squid/commit/29fd5407790e2cdf79672e8087ae7b8f1daf2576 https://wiki.debian.org/BuildingTutorial

2018-02-26 Monday

* Learned to use the elasticsearch Ruby gem to use the Elasticsearch API to implement a retention policy for elasticsearch snapshots. Much nicer than using curl or wget for complex interactions.

2018-02-27 Tuesday

* Leanred to use Rubocop to automate conformance to the Ruby style guide. Thanks, Horacio!
* https://edwinth.github.io/blog-new-things/
* Tried again to copile squid with more ports from the ubuntu source. Haven't tested it yet after deciding just to change the source directly (all the makfile stuff is mindboggling) version 7.7 is in the SquidVM!

2018-02-28 Wednesday

* Learned to use CloudWatch metrics to prove that my change had reduced the lag between a desired capacity increase and a healthy host count increase.
* Learned to use the CLoudWatch CLI to extract metric data for analysis and persistence.
* Used Mac Screen Sharing app for the first time.
* Learned that Squid's ACL helpers restart when the squid log rotates. I have not been able to find this in the documentation.

2018-03-06 Tuesday

* Learned a neat trick to delete big S3 buckets. Just set an expiry policy on everything of 1 day and wait! Let's see if it works. http://www.heystephenwood.com/2012/08/how-to-delete-large-s3-buckets-easily.html
* Started learning about unit testing and mocking while developing a script to make DNS change in AWS Route 53. Mocking AWS is not easy in Ruby, it's easier in Python.
* Continued investigating how to improve the DNS set up for the proxies. Experimented with an alternative passive failover solution using zero-weighted records.

2018-03-07 Wednesday

* Learned a hell of a lot of RSpec to do unit and integration testing in Ruby.
* Learned to use DNS programmatically through the Route 53 API. Learned that it's not very friendly to use.

2018-02-08 Thursday

* Learned more about Route 53. Learned to design and test a DNS-based active-passive failover solution.

2018-02-09

* Learned that the S3 buckets are hard to delete if versioning is involved! Delete markers are not removed even when you set it in a policy if it is mixed with other policy elements that include a date or a days count.

2018-03-14

* Learned to completely empty an S3 bucket using the empty bucket button in the console and a lot of retries and a lot of patience.
* Learned that boto3's object_versions API might be able to do the same thing, but I didn't test to prove it using mitmproxy (certificate errors)

2018-03-15

Learned to query the Sentry API to get more information from an event to analyze errors more thoroughly.

2018-03-18

* Created a new version of the EC2 SSH Config File Writer thing using test-driven development.
* Learned how to use Moto to mock the EC2 API
* Learned to use RSpec to create unit tests for the app
* Realized that bundler can create a executable gem for me - that's what i need to make this nice to use

2018-03-19

* Learned that you can access the S3 bucket of another account through the console if you use the exact console URL. Of course, you need permissions to access it first :-D The API level permissions are sufficient for the console!
* AMIs of big volumes (or just magnetic ones) (or just running ones) can take a long time to create.

2018-03-20

* Learned to use bundler to create the framework for an executable gem. [Rob Dodson]'s guide was very useful, and was completed by [Bundler]'s blog post documenting a convention change (bin/ -> exe/) make after the guide was published. In brief, use `bundler gem newgem`, edit the TODOs in the gemspec, add a class to the lib module with a hello_world method, add an exe that instantiates the the class and calls the method, and do `bundle install`, or `rake install` for system ruby, then run it with `bundle exec newgem` or just `newgem` for system ruby.

2018-03-21

* Learned to renew AWS ACM certificates using the API. This is necessary if your validation domain (hostmaster@example.com) is a superdomain of the certificate domain (site.example.com). The console doesn't let you do it.
* Wrote my first blog article on my blog!
* Write my first blog article on Ruby! See what I learned yesterday.
* Created my first gem! Converted an the ssh projecr to follow Gem conventions.
* SSH project has a name! It's called Conssh.

2018-03-22

* Learned about certificate transparancy. Discovered crt.sh, a very powerful tool for reading certificate transparency logs.
* Contributed to the project by making a case for including the expiry date in the search results. It was added by the maintainer!

2018-03-23

* Learned how to do git merges on the command line (no PRs). Merged all the changes for conssh to master.
* Learned to use Bitbucket's pipelines feature to run my tests automatically.

2018-03-26

* Learned how to submit CloudWatch metrics from Ruby.

2018-03-27

* Learned that whois parsing is absurdly hard because of the lack of a standard format.
* Ruby's whois-parser does a good job, but leaves a lot of information unavailable. https://github.com/weppos/whois-parser
* Learned about Fabricate, a gem for setting up test data in a database. Commonly used with Rails.

2018-03-28

* Submitted a ticket to the VS Code Confluence Markup plugin to add support for Jira's {noformat} tag. It is now live in version 0.1.2. https://github.com/denco/vscode-confluence-markup/issues/3

2018-04-03

* Learned that there are limitations to what you can do with argument matchers in RSpec. Sometimes it's much easier to test that the arguments are passed correctly by creating a new method whose return value is the input and then testing that its return value is correct.

2018-04-04

* Learned to use Rake to wrap a Ruby method as a task. Learned that it's also not the easiest way to do it at wakoopa, which uses the whatever rails runner.

2018-04-05

Learned to use brew doctor to fix a broken installation of csvkit
Used csvkit to join and filter exports from TransIP's domain management panel
Getting used to working in a team! Sharing changes with Horacio and Alejandro

2018-04-06

Performed a manual integration test on conssh #12
Learned about ICANN's rules for domain information updating and transfers. THere is a such a thing as domain lock which means you can't transfer until 60 days after updating contact details for example.

2018-04-07

I have my own proper domain! It's short and therefore cool.
Successfully migrated my blog to isme.es.
Strugged with creating an about me page with Hugo. Do I need to change the template to something less minimal?
Studied more Chinese. Discovered that it's actually slower to input on my Mac than on my Android.
Learned about The Chairman's Bao, a collection of graded reading of current events from HSK1 to HSK6+.

2018-05-09

Learned to gracefully remove nodes from an Elasticsearch cluster. No more yellow statuses when rebalancing! Use the shard allocation filtering and cluster level shard allocation settings.
Learned to use Athena to query ELB logs. Read "Analyzing Data in S3 using Amazon Athena" for an example of how to create the table and partition the data.
Learned to use CloudWatch math expressions to graph the combined network-in and network-out of an application's two ASGs.
